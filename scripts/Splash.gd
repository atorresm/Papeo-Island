extends Control

export (PackedScene) var next_scene
onready var anim = get_node("Anim")
onready var starun = get_node("Starun")
var is_loading = true

func _ready():
	set_process_input(true)
	anim.play("fade_in_out")
	anim.connect("finished", self, "change_to_next_scene")

func _input(event):
	if event.is_action_pressed("ui_accept"):
		next_scene()

func change_to_next_scene():
	if is_loading:
		var timer = Timer.new()
		add_child(timer)
		timer.set_wait_time(1)
		timer.connect("timeout", self, "next_scene")
		timer.start()
	else:
		next_scene()

func next_scene():
	if not is_loading:
		get_parent().add_child(next_scene.instance())
		queue_free()
