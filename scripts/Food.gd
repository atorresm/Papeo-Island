extends KinematicBody2D

const SPEED = 200
var velocity = 0
var direction = Vector2(0, 1)
var timer = 0

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	SPEED += delta * 2
	if SPEED > 300:
		SPEED = 300
	velocity = SPEED * delta * direction
	move(velocity)
	if get_pos().y > 570 - (get_node("Sprite").get_texture().get_size().y / 2):
		self.queue_free()

func set_collision_shape_size(size):
	var collision = CollisionShape2D.new()
	collision.set_shape(RectangleShape2D.new())
	self.add_child(collision)
	var transform = collision.get_shape()
	transform.set_extents(size)