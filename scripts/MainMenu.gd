extends Node2D

# pointer of the menus
var index = 0
onready var pointer = get_node("HUD").get_node("Entries").get_node("Pointer")
# animation player
onready var anim = get_node("Anim")
# sound
onready var sample_player = get_node("SamplePlayer")
# preload the game scene
var game_scn = preload("res://scenes/Game.tscn")
# flags
var credits_showing = false

func _ready():
	set_process_input(true)
	set_fixed_process(true)
	# hide the mouse
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

func _input(event):
	var is_hud_anim_finished = not anim.is_playing()
	if event.is_action_pressed("ui_up") and not event.is_echo() and not credits_showing and is_hud_anim_finished:
		sample_player.play("menu_move")
		if index != 0:
			# decrease index
			index -= 1
			pointer.set_pos(Vector2(pointer.get_pos().x, pointer.get_pos().y - 30))
		else:
			index = 2
			pointer.set_pos(Vector2(pointer.get_pos().x, pointer.get_pos().y + 60))
	if event.is_action_pressed("ui_down") and not event.is_echo() and not credits_showing and is_hud_anim_finished:
		sample_player.play("menu_move")
		if index != 2:
			index += 1
			pointer.set_pos(Vector2(pointer.get_pos().x, pointer.get_pos().y + 30))
		else:
			index = 0
			pointer.set_pos(Vector2(pointer.get_pos().x, pointer.get_pos().y - 60))
	if event.is_action_pressed("ui_accept") and not event.is_echo() and is_hud_anim_finished:
		if credits_showing:
			anim.play("credits_hide")
			credits_showing = false
		elif index == 0:
			sample_player.play("menu_select")
			# start new game
			anim.play("pointer_blink")
			yield(anim, "finished")
			load_new_game()
		elif index == 1:
			sample_player.play("menu_select")
			# show credits
			anim.play("pointer_blink")
			yield(anim, "finished")
			anim.play("credits_show")
			credits_showing = true
		elif index == 2:
			sample_player.play("menu_select")
			# quit game
			anim.play("pointer_blink")
			yield(anim, "finished")
			OS.get_main_loop().quit()

func load_new_game():
	queue_free()
	get_tree().change_scene_to(game_scn)