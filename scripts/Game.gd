extends Node2D

# Nodes
onready var UI = get_node("UI")
onready var score_counter = UI.get_node("Panel/ScoreCounter")
onready var lives = UI.get_node("Panel/Lives")
onready var ingredients_indicator = UI.get_node("Panel/Ingredients")
onready var recipe_label = UI.get_node("Panel/Preparing")
onready var player = get_node("Player")
onready var sample_player = get_node("SamplePlayer")
onready var stream_player = get_node("StreamPlayer")

# Available recipes and ingredients
# Set on Main.gd
var recipes = Globals.get("recipes")
var ingredients = Globals.get("ingredients")
var recipe_map = Globals.get("recipe_map")

# positions at which the food can spawn
# 1024 (game width) / 8 = each rail of 128 pixels
var food_rails = [0, 128, 256, 384, 512, 640, 768, 896]

# preloading
const food_scn = preload("res://scenes/Food.tscn")
const main_menu_scn = preload("res://scenes/MainMenu.tscn")
var texture_map = Globals.get("texture_map")

# control food spawning
const SECONDS_BETWEEN_FOOD_SPAWN = 0.3
const INITIAL_SECONDS_BEFORE_SPAWN = 2
var timer = 0
var initial_time_passed = false

func _ready():
	randomize()
	# game pre-setting
	scale_ingredient_sprites(3)
	Globals.set("alive", true)
	Globals.set("current_score", 0)
	set_random_recipe()
	# processing
	set_fixed_process(true)

func _fixed_process(delta):
	timer += delta
	if not initial_time_passed and timer > INITIAL_SECONDS_BEFORE_SPAWN:
		initial_time_passed = true
	if timer > SECONDS_BETWEEN_FOOD_SPAWN and initial_time_passed:
		timer = 0
		if Globals.get("alive"):
			spawn_food()

func add_score(score):
	var previous = int(score_counter.get_text())
	Globals.set("current_score", previous + score)
	score_counter.set_text("%07d" % Globals.get("current_score"))

func remove_life():
	# I know there are better ways to do this but whatever
	if lives.get_node("4").get_opacity() > 0:
		lives.get_node("4").set_opacity(0)
	elif lives.get_node("3").get_opacity() > 0:
		lives.get_node("3").set_opacity(0)
	elif lives.get_node("2").get_opacity() > 0:
		lives.get_node("2").set_opacity(0)
	elif lives.get_node("1").get_opacity() > 0:
		lives.get_node("1").set_opacity(0)
	elif lives.get_node("0").get_opacity() > 0:
		lives.get_node("0").set_opacity(0)
		# game over!
		Globals.set("alive", false)
		game_over()
	if Globals.get("alive"):
		sample_player.play("damage")

func set_random_recipe():
	var next = recipes[randi() % recipes.size()]
	# make sure the recipe doesn't repeat
	while(next.to_lower() == Globals.get("current_recipe")):
		next = recipes[randi() % recipes.size()]
	recipe_label.set_text("PREPARING " + next.to_upper())
	set_ingredients_indicator(next.to_lower())
	Globals.set("current_recipe", next.to_lower())

func set_ingredients_indicator(recipe):
	var ingredients_number = recipe_map[recipe].size()
	#make all the ingredients invisible
	for i in ingredients_indicator.get_children():
		i.set_opacity(0)
	#make visible only the number of ingredients the recipe has
	for i in range(ingredients_number):
		var sprite = ingredients_indicator.get_child(i)
		var ingredient_name = recipe_map[recipe][i]
		sprite.set_name(ingredient_name)
		sprite.set_opacity(1)
		sprite.set_texture(texture_map[ingredient_name])

func spawn_food():
	var food = food_scn.instance()
	self.add_child(food)
	food.set_z(-2)
	# set random food
	var current_food_name = ingredients[randi() % ingredients.size()]
	var food_texture = texture_map[current_food_name]
	# change the node name
	food.set_name(current_food_name.to_lower())
	# set the food texture
	food.get_node("Sprite").set_texture(food_texture)
	# scale it
	food.get_node("Sprite").scale(Vector2(3, 3))
	# scale the collision shape accordingly
	var texture_size = food_texture.get_size() * 3
	food.set_collision_shape_size(texture_size / 2)
	# add width of the texture to center it
	food.set_pos(Vector2(food_rails[randi() % food_rails.size()] + (texture_size.x), 0))

func process_collected_food(food_name):
	var current_recipe = Globals.get("current_recipe")
	var contains_collected = false
	var ingredient_collected = null
	for ingredient in recipe_map[current_recipe]:
		if food_name.findn(ingredient) != -1:
			contains_collected = true
			ingredient_collected = ingredient
			break
		else:
			contains_collected = false
	if contains_collected:
		# the collected food is in the recipe
		for i in ingredients_indicator.get_children():
			if i.get_name().findn(ingredient_collected) != -1:
				sample_player.play("ingredient_pick")
				i.set_opacity(0)
				add_score(100)
				check_recipe_is_completed()
				break
	else:
		remove_life()

func check_recipe_is_completed():
	var is_completed = false
	for i in range(ingredients_indicator.get_children().size()):
		if ingredients_indicator.get_child(i).get_opacity() == 0:
			is_completed = true
		else:
			is_completed = false
			break
	if is_completed:
		add_score(recipe_map[Globals.get("current_recipe")].size() * 100)
		set_random_recipe()

func game_over():
	# handle high score
	if Globals.get("high_score") == null or Globals.get("current_score") > Globals.get("high_score"):
		Globals.set("high_score", Globals.get("current_score"))
	player.die() # rip
	# stop game sound
	stream_player.stop()
	# death sound
	sample_player.play("death")
	UI.get_node("Anim").play("move_in_gameoverpanel")
	yield(UI.get_node("Anim"), "finished")
	# load main screen
	get_tree().change_scene_to(main_menu_scn)

func scale_ingredient_sprites(factor):
	for i in ingredients_indicator.get_children():
		i.scale(Vector2(factor, factor))