extends Control

export (PackedScene) var next_scene
var next_scene_instance = null

# Thread for loading
var load_thread = Thread.new()

func _ready():
	load_thread.start(self, "load_data")
	# display splash screen
	splash_screen()

func splash_screen():
	# load the splash screen
	next_scene_instance = next_scene.instance()
	add_child(next_scene_instance)

func load_data(args):
# Available recipes and ingredients
	var recipes = ["pizza", "burger", "salad", "sandwich", "cake", "hot-dog", "pasta", "omelette", \
	               "sushi", "ramen", "fruit salad", "paella"]
	var ingredients = ["cheese", "ham", "lettuce", "tomato", "carrot", "chocolate", "sugar", "bread", "ketchup", "potato", "fish", \
	                   "meat", "apple", "lemon", "sausage", "corn", "spaghetti", "rice", "onion", "water", "egg", "banana", 
	                   "watermelon"]
	# key: recipe, value: array with the ingredients
	var recipe_map = { "pizza":["cheese", "ham", "tomato", "bread"], 
	                   "burger":["meat", "cheese", "bread", "ketchup", "lettuce"],
	                   "salad":["lettuce", "tomato", "corn", "onion", "carrot"],
	                   "sandwich":["bread", "ham", "ketchup", "lettuce", "cheese"],
	                   "cake":["chocolate", "sugar", "bread", "apple"],
	                   "hot-dog":["sausage", "bread", "ketchup", "onion"],
	                   "pasta":["spaghetti", "tomato", "cheese", "meat"],
	                   "omelette":["egg", "potato"],
	                   "sushi":["fish", "rice"],
	                   # i know ramen isn't made with spaghetti, it's just for the sprite
	                   "ramen":["spaghetti", "water", "egg", "onion", "meat"],
	                   "fruit salad":["apple", "banana", "watermelon"],
	                   "paella":["rice", "lemon", "meat", "tomato"]}
	Globals.set("recipes", recipes)
	Globals.set("ingredients", ingredients)
	Globals.set("recipe_map", recipe_map)
	Globals.set("texture_map", {})
	preload_food_sprites()
	next_scene_instance.is_loading = false
	# keep a reference to the load thread
	Globals.set("load_thread", load_thread)

func preload_food_sprites():
	var texture_map = Globals.get("texture_map")
	for i in Globals.get("ingredients"):
		var path = "res://assets/food/" + i + ".png"
		var texture = load(path)
		if texture != null:
			texture_map[i] = texture
		else:
			print("The " + i + " texture couldn't be loaded!")
