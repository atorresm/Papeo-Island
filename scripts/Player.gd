extends KinematicBody2D

var input_direction = 0
var direction = 0
var velocity = 0
const SPEED = 800
onready var anim = get_node("Anim")

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	var pos = get_pos()
	set_pos(bound_pos(pos))
	
	if input_direction != null:
		direction = input_direction
	if Input.is_action_pressed("ui_left"):
		input_direction = -1
	elif Input.is_action_pressed("ui_right"):
		input_direction = 1
	else:
		input_direction = 0

	#movement
	velocity = SPEED * delta * direction
	if Globals.get("alive"):
		move(Vector2(velocity, 0))

func bound_pos(pos):
	var sprite_size_offset = get_node("Sprite").get_texture().get_size().x / 2
	if pos.x > 1024 - sprite_size_offset:
		pos.x = 1024 - sprite_size_offset
	if pos.x < 0 + sprite_size_offset:
		pos.x = 0 + sprite_size_offset
	return pos

func _on_Area2D_body_enter(body):
	if Globals.get("alive"):
		get_node("..").process_collected_food(body.get_name())
	body.queue_free()

func die():
	anim.play("die")