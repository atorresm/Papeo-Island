# Papeo Island  
Prepare delicious recipes in a wonderful island and reach the high score!  
Second place in the [Food Jam 2017](https://itch.io/jam/food-jam)!  
Made with Godot Engine.  
Check my other games on [itch.io](https://papaya-games.itch.io/)

## License  
>This program is Free Software: You can use, study share and improve it at your will. Specifically you can redistribute and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See LICENSE file for the full text of the license.

